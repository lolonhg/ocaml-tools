type 'a o3b = ('a, Internal_BArray.t) O3.o3
type 'a o3s = ('a, BinUtils.stream) O3.o3s

let unit = (Internal_ToB.unit, Internal_OfB.unit)
let option (dump, load) = (Internal_ToB.option dump, Internal_OfB.option load)
let bool = (Internal_ToB.bool, Internal_OfB.bool)
let barray = (Internal_ToB.barray, Internal_OfB.barray)
let list (dump, load) = (Internal_ToB.list dump, Internal_OfB.list load)
let array (dump, load) = (Internal_ToB.array dump, Internal_OfB.array load)
let none_list (dump, load) = (Internal_ToB.none_list dump, Internal_OfB.none_list load)
let int = (
  Internal_ToB.int,
   Internal_OfB.int
)
let sized_int size = (
  Internal_ToB.sized_int size,
   Internal_OfB.sized_int size
)
let closure ((dump, load) : ('a, 's) O3.o3s) : ('a, Internal_BArray.t) O3.o3 =
  (Internal_ToB.closure dump, Internal_OfB.closure load)

let bool_option_list = (
  Internal_ToB.bool_option_list,
   Internal_OfB.bool_option_list
)

let pair (dA, lA) (dB, lB) = (
  Internal_ToB.pair dA dB,
   Internal_OfB.pair lA lB
)

let trio (dA, lA) (dB, lB) (dC, lC) = (
  Internal_ToB.trio dA dB dC,
   Internal_OfB.trio lA lB lC
)

let c2 (d0, l0) (d1, l1) = (
  Internal_ToB.c2 d0 d1,
   Internal_OfB.c2 l0 l1
)
let c3 (d0, l0) (d1, l1) (d2, l2) = (
  Internal_ToB.c3 d0 d1 d2,
   Internal_OfB.c3 l0 l1 l2
)
let c4 (d0, l0) (d1, l1) (d2, l2) (d3, l3) = (
  Internal_ToB.c4 d0 d1 d2 d3,
   Internal_OfB.c4 l0 l1 l2 l3
)
