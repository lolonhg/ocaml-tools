let unit : unit BinUtils.load = fun stream -> (), stream

let map f load stream =
  let item, stream = load stream in
  (f item), stream

let c2 load0 load1 = function
  | false::stream -> Poly.(
    let elem, stream = load0 stream in
    (C2_0 elem), stream
  )
  | true ::stream -> Poly.(
    let elem, stream = load1 stream in
    (C2_1 elem), stream
  )
  | _ -> failwith "[ocaml-tools/binLoad:c2] parsing error"

let c3 load0 load1 load2 = function
  | false::stream -> Poly.(
    let elem, stream = load0 stream in
    (C3_0 elem), stream
  )
  | true ::false::stream -> Poly.(
    let elem, stream = load1 stream in
    (C3_1 elem), stream
  )
  | true ::true ::stream -> Poly.(
    let elem, stream = load2 stream in
    (C3_2 elem), stream
  )
  | _ -> failwith "[ocaml-tools/binLoad:c3] parsing error"

let c4 load0 load1 load2 load3 = function
  | false::false::stream -> Poly.(
    let elem, stream = load0 stream in
    (C4_0 elem), stream
  )
  | false::true ::stream -> Poly.(
    let elem, stream = load1 stream in
    (C4_1 elem), stream
  )
  | true ::false::stream -> Poly.(
    let elem, stream = load2 stream in
    (C4_2 elem), stream
  )
  | true ::true ::stream -> Poly.(
    let elem, stream = load3 stream in
    (C4_3 elem), stream
  )
  | _ -> failwith "[ocaml-tools/binLoad:c4] parsing error"

let choice loadF loadT = function
  | b::stream -> (if b then loadT else loadF) stream
  | _      -> failwith "[ocaml-tools/binLoad:choice] parsing error"

let option load = choice
  (fun stream -> None, stream)
  (fun stream ->
    let x, stream = load stream in
    Some x, stream)

(* copied from MyList *)
let hdtl_nth n liste =
  let rec aux carry = function
    | 0, next -> (List.rev carry, next)
    | n, [] -> assert false
    | n, head::tail -> aux (head::carry) (n-1, tail)
  in
  assert (n>=0);
  assert ((List.length liste)>=n);
  let head, tail = aux [] (n, liste) in
  assert((List.length head)=n);
  head, tail

let sized_barray size stream =
  let x, stream = hdtl_nth size stream in
  (Internal_BArray.of_bool_list x, stream)

let none_list load =
  let rec aux carry stream =
    let opelem, stream = load stream in
    match opelem with
    | None -> carry, stream
    | Some elem -> aux (elem::carry) stream
  in aux []

let while_list func init =
  let rec aux carry state stream =
    let op_elem_op_state, stream = func stream in
    match op_elem_op_state with
      | None -> (List.rev carry), stream
      | Some (elem, op_state) -> match op_state with
        | None -> (List.rev (elem::carry)), stream
        | Some state -> aux (elem::carry) state stream
  in aux [] init

let sized_list (load : 'a BinUtils.load) (size : int) : 'a list BinUtils.load =
  (* print_string "sized_list"; print_newline(); SUtils.print_stream stream; print_newline(); *)
  let rec aux carry stream = function
    | 0 -> carry, stream
    | n ->
    (
      let head, stream = load stream in
      aux (head::carry) stream (n-1)
    )
  in (fun stream -> aux [] stream size)

let list (load : 'a BinUtils.load) : 'a list BinUtils.load=
  let rec aux carry = function
    | false::stream  -> carry, stream
    | true::stream  ->
      let elem, stream = load stream in
      aux (elem::carry) stream
    | _      -> failwith "[ocaml-tools/binLoad:list] parsing error"
  in aux []

let bool : bool BinUtils.load = function
  | b::stream -> b, stream
  | _         -> failwith "[ocaml-tools/binLoad:bool] parsing error"

let unary : int BinUtils.load =
  let rec aux n = function
    | [] -> failwith "[ocaml-tools/binLoad:unary] parsing error"
    | head::stream -> if head then (n, stream) else (aux (n+1) stream)
  in aux 0

let sized_int size : int BinUtils.load = fun stream ->
  (* print_string "sized_int"; print_newline(); SUtils.print_stream stream; print_newline(); *)
  let l, stream = hdtl_nth size stream in
  let rec aux x = function
    | [] -> x
    | head::tail -> aux (x*2 + (if head then 1 else 0)) tail
  in (aux 0 l, stream)

let int : int BinUtils.load = fun stream ->
  let k, stream = unary stream in
  sized_int k stream

let barray stream =
  let s, stream = int stream in
  let x, stream = sized_barray s stream in
  (x, stream)

let pair (loadA : 'a BinUtils.load) (loadB : 'b BinUtils.load) : ('a * 'b) BinUtils.load = fun stream ->
  let a, stream = loadA stream in
  let b, stream = loadB stream in
  (a, b), stream

let ( * ) = pair

let trio loadA loadB loadC stream =
  let a, stream = loadA stream in
  let b, stream = loadB stream in
  let c, stream = loadC stream in
  (a, b, c), stream

let quad loadA loadB loadC loadD stream =
  let a, stream = loadA stream in
  let b, stream = loadB stream in
  let c, stream = loadC stream in
  let d, stream = loadD stream in
  (a, b, c, d), stream

let closure (load : 'a BinUtils.load) bitv : 'a =
  let objet, stream = bitv |> Internal_BArray.to_bool_list |> load in
  assert(stream = []);
  objet

let list (load : 'a BinUtils.load) : 'a list BinUtils.load = fun stream ->
  let size, stream = int stream in
  sized_list load size stream

let array (load : 'a BinUtils.load) : 'a array BinUtils.load = fun stream ->
  let liste, stream = list load stream in
  (Array.of_list liste, stream)

let bool_option_list = none_list (function
  | b0::b1::stream ->
  ((
    if b0
    then if b1
      then None
      else (Some None)
    else (Some(Some b1))
  ), stream)
  | _ -> failwith "[ocaml-tools/binLoad:unary] parsing error")

let o3 = snd
