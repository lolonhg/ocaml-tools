let rec sorted_rec x = function
  | [] -> true
  | y::t -> (x<y)&&(sorted_rec y t)

(* return true iff the input list is strictly increasing *)
(* Time Complexity O(n) *)
let sorted = function
  | [] -> true
  | x::t -> sorted_rec x t

(* return true iff the input list is strictly increasing *)
(* checks that the first element is greater than 0 *)
(* Time Complexity O(n) *)
let sorted_nat = function
  | [] -> true
  | x::t -> (0<=x)&&(sorted_rec x t)

let sort (l:'a list) : 'a list =
  List.sort_uniq Stdlib.compare l

let sort (l:'a list) : 'a list =
  List.sort_uniq Stdlib.compare l

(* merges two already sorted list and returns a sorted list *)
(* removes duplicates *)
(* Time Complexity O(nX + nY) *)
(* Tail-Recursive *)
let union lX lY : _ list =
  let rec aux carry = function
  | ([], l) | (l, []) ->
    List.rev_append carry l
  | (((x::x') as lx), ((y::y') as ly)) -> if x = y
    then aux (x::carry) (x', y')
    else if x < y
    then aux (x::carry) (x', ly)
    else aux (y::carry) (lx, y')
  in
  aux [] (lX, lY)

let minus (lX:'a list) (lY:'a list) : 'a list =
(* returns [lX] - [lY] *)
(* Time Complexity O(nX+nY) *)
  let rec aux carry = function
    | ([], _) -> (List.rev carry)
    | (x, []) -> (List.rev_append carry x)
    | (x::x', y::y') -> if x = y
      then (aux carry (x', y'))
      else if x < y
      then (aux (x::carry) (x', y::y'))
      else (aux carry (x', y'))
  in aux [] (lX, lY)

let inter lX lY =
(* return [lX] inter [lY] *)
(* Time Complexity O(nX+nY) *)
  let rec aux carry = function
    | ([], _) | (_, []) ->
      (List.rev carry)
    | (x::x', y::y') -> if x = y
      then (aux (x::carry) (x', y'))
      else (aux carry (if x < y
      then (x', y::y')
      else (x::x', y')))
  in aux [] (lX, lY)

let nointer lX lY =
(* returns (lX inter lY) = emptyset *)
(* Time Complexity O(nX+nY) *)
  let rec aux = function
    | ([], _) | (_, []) -> false
    | (x::x', y::y') ->
      (x<>y)&&(aux (if x < y
      then (x', y::y')
      else (x::x', y')))
  in aux (lX, lY)

let subset_of lX lY =
(* return true iff [lX] is a subset of [lY] *)
(* Time Complexity O(nX+nY) *)
  let rec aux = function
    | ([], _) -> true
    | (_, []) -> false
    | (x::x', y::y') -> if x = y
      then (aux (x', y'))
      else ((x<y)&&(aux (x', y::y')))
  in aux (lX, lY)
