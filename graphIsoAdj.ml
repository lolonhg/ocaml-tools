let (++) = SetList.union
let (--) = SetList.minus

type ('t, 'b) node = {
  index : int;
  edges : int list;
(* used to discrimate nodes when merging *)
  tag : 't;
(* does not affect merging condition, bags are merged on merging *)
  bag : 'b;
}

type ('t, 'b) graph = ('t, 'b) node option array

let string_of_int_list l : string =
  MyList.string_of_list string_of_int l

let print_int_list l : unit =
  print_string(string_of_int_list l)

let string_of_int_list_list l : string =
  MyList.string_of_list string_of_int_list l

let string_of_node string_of_tag string_of_bag node =
   "{index = "^(string_of_int node.index)^
  "; edges = "^(string_of_int_list node.edges)^
  "; tag = "^(string_of_tag node.tag)^
  "; bag = "^(string_of_bag node.bag)^"}"

let print_node string_of_tag string_of_bag node : unit =
  print_string(string_of_node string_of_tag string_of_bag node)

let print_graph string_of_tag string_of_bag graph : unit =
  print_string "[|"; print_newline();
  Array.iter (function
    | None -> print_string "\tNone;\n";
    | Some node -> (
      print_string "\tSome ";
      print_node string_of_tag string_of_bag node;
      print_string ";";
      print_newline()
    )
  ) graph;
  print_string "|]"; print_newline()

let strict_neighbourgs (graph:('t, 'b)graph) (node:('t, 'b)node) : ('t, 'b) node list =
  List.map (fun x -> match graph.(x) with
    | None -> assert false
    | Some node -> node) node.edges

let internal_elim_node (graph:('t, 'b) graph) (node:('t, 'b) node) =
  graph.(node.index) <- None;
  List.iter
    (fun (node':('t, 'b) node) ->
      let edges : int list = SetList.minus (node'.edges:int list) [(node.index:int)] in
      graph.(node'.index) <- Some {node' with edges})
    (strict_neighbourgs graph node)

let merging (eq_tag:'t->'t->bool) (merge_bag:'b->'b->'b) (graph:('t, 'b) graph) : unit =
  (* check for merging nodes *)
  let n = Array.length graph in
  for x = 0 to n-1
  do
    match graph.(x) with
    | None -> ()
    | Some nodeX -> (
      let bag    = ref nodeX.bag in
      let edges  = ref nodeX.edges in
      for y = x+1 to n-1
      do
        assert(x<y);
        match graph.(y) with
        | None -> ()
        | Some nodeY -> (
          if !edges ++ [nodeX.index] = nodeY.edges ++ [nodeY.index] &&
            (eq_tag nodeX.tag nodeY.tag)
          then (
            internal_elim_node graph nodeY;
            bag    := merge_bag !bag nodeY.bag;
            edges  := !edges -- [nodeY.index];
          )
        )
      done;
      graph.(x) <- Some {nodeX with edges = !edges; bag = !bag}
    )
  done

(* [connex_components graph] return decomposition of the
     graph [graph] into connex components.

   warning : if [graph] is empty or made of a single component
     then [connex_components graph = [graph]] to avoid copy if not necessary
 *)

let connex_components (graph:('t, 'b) graph) : ('t, 'b) graph list =
  let n = Array.length graph in
  let uf = UnionFind.init n in
  for i = 0 to n-1
  do match graph.(i) with
    | None -> uf.(i) <- -1;
    | Some node -> (
      List.iter (UnionFind.union uf node.index) node.edges
    )
  done;
  let nbcomp = MyArray.counti (=) uf in
  if nbcomp <= 1 then [graph] else (
    let comps = Array.init nbcomp (fun _ -> Array.make n None) in
    let indexes = MyArray.indexesi (=) uf in
    let rev_indexes = Array.make n (-1) in
    Array.iteri (fun i x -> rev_indexes.(x) <- i) indexes;
    for i = 0 to n-1
    do
      if uf.(i) >= 0 then (
        let j = UnionFind.find uf i in
        comps.(rev_indexes.(j)).(i) <- graph.(i)
      )
    done;
    comps |> Array.to_list
  )

let check eq_tag graph : bool =
  let state = ref true in
  let add (b:bool) : unit = state := !state && b in
  Array.iteri (fun index -> function
    | None -> ()
    | Some node -> (
      add (index = node.index);
      add (not(List.mem index node.edges));
      List.iter (fun index' -> match graph.(index') with
          | None       -> (add false)
          | Some node' -> (
            add (List.mem index node'.edges);
            let neigh = node.edges ++ [node.index]
            and neigh' = node'.edges ++ [node'.index]
            and tagdiff = not(eq_tag node.tag node'.tag) in
            add (neigh <> neigh' || tagdiff)
          )
        ) node.edges
    )
  ) graph;
  !state
