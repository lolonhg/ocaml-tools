(* return true iff the input list is sorted (i.e. strictly increasing)*)
(* Time Complexity O(n) *)
val sorted : 'a list -> bool
(* return true iff the input list is sorted (i.e. strictly increasing)
   and all elements are positive
 *)
(* Time Complexity O(n) *)
val sorted_nat : int list -> bool
val sort : 'a list -> 'a list

(* merges two already sorted list and returns a sorted list *)
(* removes duplicates *)
(* Time Complexity O(nX + nY) *)
(* Tail-Recursive *)
val union : 'a list -> 'a list -> 'a list

(* returns [lX] - [lY] *)
(* Time Complexity O(nX+nY) *)
val minus : 'a list -> 'a list -> 'a list

(* return [lX] inter [lY] *)
(* Time Complexity O(nX+nY) *)
val inter : 'a list -> 'a list -> 'a list

(* returns (lX inter lY) = emptyset *)
(* Time Complexity O(nX+nY) *)
val nointer : 'a list -> 'a list -> bool

(* return true iff [lX] is a subset of [lY] *)
(* Time Complexity O(nX+nY) *)
val subset_of : 'a list -> 'a list -> bool
