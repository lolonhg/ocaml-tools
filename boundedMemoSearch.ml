let default_size = 10000

type ('p, 'b, 's) t = {
  table : ('p, ('b * 's, 'b) result) Hashtbl.t;
  bound_compare : 'b -> 'b -> int;
  mutable hitCnt : int;
  mutable clcCnt : int;
}

let create cmp n = {
  table = Hashtbl.create n;
  bound_compare = cmp;
  hitCnt = 0;
  clcCnt = 0;
}

let replace mem p r1 =
  try
    let r2 = Hashtbl.find mem.table p in
    match r1, r2 with
    | Ok (b1, s1), Ok (b2, s2) -> (
      assert(mem.bound_compare b1 b2 = 0);
      Some (b2, s2)
    )
    | Ok (b1, s1), Error b2 -> (
      assert(mem.bound_compare b1 b2 > 0);
      Hashtbl.replace mem.table p r1;
      Some (b1, s1)
    )
    | Error b1, Ok (b2, s2) -> (
      assert(mem.bound_compare b1 b2 < 0);
      Some (b2, s2)
    )
    | Error b1, Error b2 -> (
      if mem.bound_compare b1 b2 < 0
      then (Hashtbl.replace mem.table p r1);
      None
    )
  with
  | Not_found -> (
    Hashtbl.add mem.table p r1;
    match r1 with
    | Ok (b1, s1) -> Some (b1, s1)
    | Error _ -> None
  )

let apply (mem:('p,'b,'s) t) (fonc:'b->'p->('b*'s)option) (b0:'b) (p0:'p) : ('b*'s) option =
  try
    match Hashtbl.find mem.table p0 with
    | Ok (b1, s1) -> (
      mem.hitCnt <- succ mem.hitCnt;
      if mem.bound_compare b1 b0 < 0
      then Some (b1, s1)
      else None

    )
    | Error b1 -> (
      if mem.bound_compare b1 b0 < 0
      then (
        mem.clcCnt <- succ mem.clcCnt;
        replace mem p0 (match fonc b0 p0 with
          | Some (b1, s1) -> Ok(b1, s1)
          | None -> Error b0)
      ) else (
        mem.hitCnt <- succ mem.hitCnt;
        None
      )
    )
  with
  | Not_found -> (
    mem.clcCnt <- succ mem.clcCnt;
    replace mem p0 (match fonc b0 p0 with
      | Some (b1, s1) -> Ok(b1, s1)
      | None -> Error b0)
  )

let print_stats mem =
  print_string ">> Statistics for BoundedMemoSearch(BMS):";
  print_string "\n\t>length:\t\t=>";
  print_int (Hashtbl.length (mem.table));
  print_string ".\n\t> HitCnt:\t\t=>";
  print_int mem.hitCnt;
  print_string ".\n\t> ClcCnt:\t\t=>";
  print_int mem.clcCnt;
  print_string "."; print_newline()

module STD = STools.ToSTree

let dump_stats mem = Tree.Node [
    Tree.Node [Tree.Leaf "length:"; STD.int (Hashtbl.length (mem.table))];
    Tree.Node [Tree.Leaf "hit count:"; STD.int mem.hitCnt];
    Tree.Node [Tree.Leaf "clc count:"; STD.int mem.clcCnt]
  ]

let make cmp n =
  let mem = create cmp n in
  ( mem, apply mem )

module BNat = BTools.BArray.Nat

let rec bnat_propa call bound state ?(best=None) = function
  | [] -> best
  | head::tail -> match call bound state head with
    | Some(val1, sol1) as best ->
      bnat_propa call BNat.(pred val1) state ~best tail
    | None ->
      bnat_propa call bound            state ~best tail
