let rev (a:'a array) : 'a array =
  let n = Array.length a in
  Array.init n (fun i -> a.(n-1-i))

let count f a =
  let cnt = ref 0 in
  Array.iteri (fun i x -> if f x then incr cnt) a;
  !cnt

let counti f a =
  let cnt = ref 0 in
  Array.iteri (fun i x -> if f i x then incr cnt) a;
  !cnt

let indexes f a : int array = Array.of_list (MyList.indexes f (Array.to_list a))
let indexes_true a : int array = Array.of_list (MyList.indexes_true (Array.to_list a))
let indexesi f a : int array = Array.of_list (MyList.indexesi f (Array.to_list a))

let flatten (a:'a array array) : 'a array =
  Array.of_list (List.flatten (List.map Array.to_list (Array.to_list a)))

let unop (opa:'a option array) : 'a array =
  Array.map Tools.unop opa

let for_all2 p a1 a2 =
  let len = Array.length a1 in
  if len <> Array.length a2
    then (raise (Invalid_argument "MyArray.for_all2"));
  let rec aux a1 a2 i =
    (i >= len) || (
      (p (Array.unsafe_get a1 i)
         (Array.unsafe_get a2 i))
      && (aux a1 a2 (succ i))
    )
  in aux a1 a2 0

let firstSome2 (opfun:'a -> 'b -> 'c option)
  (aa:'a array) (bb:'a array) : 'c option =
  let len = Array.length aa in
  if len <> Array.length bb
    then invalid_arg "MyArray.firstSome2 invalif argument";
  let rec aux i =
    if i < len
      then match opfun
        (Array.unsafe_get aa i)
        (Array.unsafe_get bb i) with
        | Some some -> Some some
        | None -> aux (succ i)
      else None
  in aux 0

let sum (mapfun : 'a -> int) (a:'a array) : int =
  let sum = ref 0 in
  for i = 0 to Array.length a - 1
  do
    sum := !sum + (mapfun(Array.unsafe_get a i))
  done;
  !sum

let assoc (x:'a) (xy:('a * 'b) array) : 'b =
  let len : int = Array.length xy in
  let rec aux (i:int) : 'b =
    if i < len then (
      let (x', y') = Array.unsafe_get xy i in
      if x = x' then y' else (aux (succ i))
    ) else (assert false)
  in aux 0
