open Extra

let flatten ll =
  let rec aux carry = function
    | [] -> List.rev carry
    | l::ll -> aux (List.rev_append l carry) ll
  in aux [] ll

let of_htbl = HuffmanCoding.of_htbl
let to_htbl = HuffmanCoding.to_htbl

let partition part liste =
  let rec aux l0 l1 = function
    | [] -> (List.rev l0, List.rev l1)
    | head::tail -> match part head with
      | Ok obj -> aux l0 (obj::l1) tail
      | Error obj -> aux (obj::l0) l1 tail
  in aux [] [] liste

let bool_partition bl l =
  let rec aux l0 l1 = function
    | ([], []) -> List.(rev l0, rev l1)
    | (b::bl, x::l) -> (if b
      then aux l0 (x::l1) (bl, l)
      else aux (x::l0) l1 (bl, l)
    )
    | _ -> assert false
  in aux [] [] (bl, l)

let list_init n f =
  assert(n>=0);
  let rec aux carry = function
    | 0 -> carry
    | n -> aux ((f(n-1))::carry) (n-1)
  in aux [] n
let init = list_init
(*
let list_init = List.init
  [@@ocaml.deprecated "List.init instead."]

let init = List.init
  [@@ocaml.deprecated "List.init instead."]
*)

let catlist (cat: 'a) : 'a list list -> 'a list = function
  | [] -> []
  | [x] -> x
  | x::x' -> x@(flatten (List.map (fun (x:'a list) -> cat::x) x'))

let option_cons = function
  | None -> (fun l -> l)
  | Some x -> (fun l -> (x::l))

let catmap f l = flatten (List.map f l)

let list_of_oplistv0 opel = List.fold_right option_cons opel []

let list_of_oplistv1 oplist =
  oplist
  |> List.filter (function Some _ -> true | None -> false)
  |> List.map (function Some x -> x | None -> assert false)

let list_of_oplistv3 =
  let rec aux = function
    | [] -> []
    | None::oplist' -> aux oplist'
    | (Some item)::oplist' -> item::(aux oplist')
  in aux

let list_of_oplistv4 : 'a option list -> 'a list =
  let rec aux carry = function
    | [] -> List.rev carry
    | None::oplist' -> aux carry oplist'
    | (Some item)::oplist' -> aux (item::carry) oplist'
  in (fun l -> aux [] l)

let list_of_oplist = list_of_oplistv4

let unop = list_of_oplist

let opmap opfun liste =
  let rec aux carry = function
    | [] -> List.rev carry
    | head::tail -> match opfun head with
      | Some head -> aux (head::carry) tail
      | None      -> aux        carry  tail
  in aux [] liste
let opmap2 opfun liste1 liste2 = list_of_oplist (List.map2 opfun liste1 liste2)

let sum = List.fold_left (+) 0

let count f l = sum(l||>(fun x -> if f x then 1 else 0))
let counti f l = sum(List.mapi (fun i x -> if (f i x) then 1 else 0)l)

let count_true = count (fun x -> x)

let indexes f l = unop(List.mapi (fun i x -> if f x then Some i else None) l)
let indexes_true l = indexes (fun x -> x) l

let indexesi f l =
  let rec aux carry i = function
    | [] -> List.rev carry
    | x::x' -> aux (if f i x then (i::carry) else carry) (i+1) x'
  in aux [] 0 l

let list_index item =
  let rec aux pos = function
    | [] -> None
    | head::tail ->
      if item=head
      then Some pos
      else (aux (pos+1) tail)
  in aux 0

let ifind (p : 'a -> 'b option) : 'a list -> (int * 'b) option =
  let rec aux pos = function
    | [] -> None
    | head::tail -> match p head with
      | None -> aux (pos+1) tail
      | Some obj -> Some(pos, obj)
  in aux 0

(* extract the longest prefix of [l] for which all elements satifiy predicate [p].
 *)
let find_prefix (p:'a -> bool) (l:'a list) : 'a list * 'a list =
  let rec aux carry = function
    | [] -> (List.rev carry, [])
    | (h::t) as l ->
      if p h then aux (h::carry) t
      else (List.rev carry, l)
  in aux [] l

(* extract the longest prefix [pref] of [l] such that :
    - forall x\in l, f x = f(List.nth pref 0) (if pref <> [])
   The optional predicate [p] is only checked once on the first element, if its false returns [([], l)]
 *)
let find_onehash_prefix ?(p=(fun _ -> true)) (f:'a -> 'b) (l:'a list) : 'a list * 'a list =
  match l with
  | [] -> ([], [])
  | (h::t) as l ->
    if p h
    then
    (
      let fh = f h in
      let heads, tails = find_prefix (fun x -> f x = fh) t in
      (h::heads, tails)
    )
    else ([], l)

let index p =
  let rec aux pos = function
    | [] -> None
    | head::tail ->
      if p head
      then Some pos
      else (aux(pos+1) tail)
  in aux 0

let ntimes ?(carry=[]) x n =
  let rec aux carry = function
    | 0 -> carry
    | n -> aux (x::carry) (n-1)
  in
  assert(n>=0);
  aux carry n

let make n x = ntimes x n

let ncopy x n =
  let rx = List.rev x in
  let rec aux carry = function
    | 0 -> carry
    | n -> aux (List.rev_append rx carry) (n-1)
  in aux [] n

let listfilter filt flist =
  let rec aux filt flist plist = match filt, flist with
    | [], []  -> List.rev plist
    | [], _    -> failwith "[ocaml-tools/myList:listfilter] - non coherent : 0"
    | _, []    -> failwith "[ocaml-tools/myList:listfilter] - non coherent : 1"
    | true ::filt, x::flist  -> aux filt flist (x::plist)
    | false::filt, x::flist  -> aux filt flist plist
  in aux filt flist []

let consensus0v1 merge =
  let rec aux p (f0, p0) (f1, p1) = match f0, f1 with
    | [], []  -> (List.rev p, List.rev p0, List.rev p1)
    | [], _    -> failwith "[ocaml-tools/myList:consensus0v1] - non coherent : 0"
    | _, []    -> failwith "[ocaml-tools/myList:consensus0v1] - non coherent : 1"
    | x0::f0, x1::f1 -> let x, x0, x1 = merge x0 x1 in
      aux (x::p) (f0, option_cons x0 p0) (f1, option_cons x1 p1)
  in (fun f0 f1 -> aux [] (f0, []) (f1, []))

let consensus0v2 merge f0 f1 =
  let p, p01 = List.map2 (fun x y -> let x, y, z = merge x y in x, (y, z)) f0 f1 |> List.split in
  let p0, p1 = List.split p01 in
  (p, list_of_oplist p0, list_of_oplist p1)

let consensus0 = consensus0v1

let consensusi0 merge =
  let rec aux i p (f0, p0) (f1, p1) = match f0, f1 with
    | [], []  -> (List.rev p, List.rev p0, List.rev p1)
    | [], _    -> failwith "[ocaml-tools/myList:consensusi0] - non coherent : 0"
    | _, []    -> failwith "[ocaml-tools/myList:consensusi0] - non coherent : 1"
    | x0::f0, x1::f1 -> let x, x0, x1 = merge i x0 x1 in
      aux (i+1) (x::p) (f0, option_cons x0 p0) (f1, option_cons x1 p1)
  in (fun f0 f1 -> aux 0 [] (f0, []) (f1, []))

let consensus merge =
  let rec aux (e, p) (e0, f0, p0) (e1, f1, p1) = match f0, f1 with
    | [], []  -> ((e, List.rev p), (e0, List.rev p0), (e1, List.rev p1))
    | [], _    -> failwith "[ocaml-tools/myList:consensus] - non coherent : 0"
    | _, []    -> failwith "[ocaml-tools/myList:consensus] - non coherent : 1"
    | x0::f0, x1::f1 -> let (e, x), (e0, x0), (e1, x1) = merge e (e0, x0) (e1, x1) in
      aux (e, x::p) (e0, f0, option_cons x0 p0) (e1, f1, option_cons x1 p1)
  in (fun e (e0, f0) (e1, f1) -> aux (e, []) (e0, f0, []) (e1, f1, []))

let onehash_check hash hashed = List.for_all (fun item -> hashed = (hash item))
let onehash hash = function
  | [] -> true
  | head::tail -> onehash_check hash (hash head) tail

let get_onehash hash = function
  | [] -> None
  | head::tail ->
    let hashed = hash head in
    assert(onehash_check hash hashed tail);
    Some hashed

let lists_length_check size lists = onehash_check List.length size lists
let lists_length lists = onehash List.length lists
let lists_get_length lists = get_onehash List.length lists

let nth_pop (l: 'a list) n : 'a option * ('a list) =
  let rec aux carry x = function
    | [] -> (None, l)
    | head::tail ->
      if x = 0 then (
        (Some head, List.rev_append carry tail)
      ) else (
        aux (head::carry) (x-1) tail
      )
  in aux [] n l

let iremove (p: 'a -> 'b option) (l: 'a list) : (int * 'b) option * ('a list) =
  let rec aux carry pos = function
    | [] -> None, l
    | head::tail -> match p head with
      | None -> aux (head::carry) (pos+1) tail
      | Some obj ->
        (Some(pos, obj), List.rev_append carry tail)
  in aux [] 0 l

let hdtl = function
  | []  -> assert false
  | x::y  -> x, y

let map_hdtl vects =
  List.split (List.map hdtl vects)

let hdtl_nth n liste =
  let rec aux carry = function
    | 0, next -> (List.rev carry, next)
    | n, [] -> assert false
    | n, head::tail -> aux (head::carry) (n-1, tail)
  in
  assert (n>=0);
  assert ((List.length liste)>=n);
  let head, tail = aux [] (n, liste) in
  assert((List.length head)=n);
  head, tail

let list_add_partial_to_support default support partial =
  let auxend support = List.map (fun liste -> default::liste) support in
  let rec aux carry = function
    | support, [] -> (List.rev carry)@(auxend support)
    | [], _    -> assert false
    | head_support::tail_support, head_partial::tail_partial ->
      aux ((head_partial::head_support)::carry) (tail_support, tail_partial)
  in aux [] (support, partial)

let list_transpose_partial_matrix default size partial_matrix =
  let rec parcours support = function
    | [] -> List.map List.rev support
    | head::tail -> parcours (list_add_partial_to_support default support head) tail
  in
  assert(size>=0);
  parcours (list_init size (fun _ -> [])) partial_matrix

let member x l = List.mem x l
  [@@ocaml.deprecated "List.mem instead."]

let int_of_boollist =
  let rec aux acc = function
    | [] -> acc
    | head::tail -> aux (2*acc+(if head then 1 else 0)) tail
  in aux 0

let rec list_uniq = function
  | []  -> []
  | [a]  -> [a]
  | a::b::next ->  (
    if a=b
    then list_uniq (b::next)
    else a::(list_uniq (b::next))
          )

let string_of_list string_of liste = String.concat "" ["["; String.concat "; " (List.map string_of liste); "]"]

let print_list string_of liste = print_string (string_of_list string_of liste)

let list_map_z1 func =
  let rec aux item = function
    | [] -> []
    | head::tail -> (func item head)::(aux head tail)
  in function
    | [] -> []
    | head::tail -> aux head tail

let indexify filter =
  let rec aux index = function
    | [] -> []
    | head::tail -> if (filter head)
      then ((Some index)::(aux (index+1) tail))
      else (None::(aux index tail))
  in aux 0

let indexify_true = indexify (fun x -> x)

let foldmap func check init =
  let rec aux carry fold = function
    | [] -> (List.rev carry, fold)
    | head::tail ->
    (
      (*(check fold) is true*)
      let head', fold' = func fold head in
      assert(check fold');
      aux (head'::carry) fold' tail
    )
  in
  assert(check init);
  aux [] init

let opfoldmap func check init =
  let rec aux carry fold = function
    | [] -> (List.rev carry, fold)
    | head::tail ->  (
      (*(check fold) is true*)
      let head', fold' = func head fold in
      assert(check fold');
      match head' with
        | Some head' -> aux (head'::carry) fold' tail
        | None -> aux carry fold' tail
            )
  in
  assert(check init);
  aux [] init

let list_iterative_reduction func =
  let rec aux carry obj =
    let opitem, opobj' = func obj in
    let carry' = option_cons opitem carry in
    match opobj' with
      | Some obj'  -> aux carry' obj'
      | None    -> List.rev carry'
  in aux []

let last =
  let rec aux carry = function
    | []      -> assert false
    | [x]      -> List.rev carry, x
    | head::tail  -> aux (head::carry) tail
  in function
    | [] -> assert false
    | liste -> aux [] liste

let setnth liste x e =
  assert(0 <= x && x < (List.length liste));
  List.mapi (fun i -> if i = x then (fun _ -> e) else (fun y -> y)) liste

let list_min (l:'a list) : 'a * int =
  let rec aux v k0 k = function
    | [] -> (v, k)
    | v0::t0 ->
      if v0 < v
        then aux v0 (succ k0) k0 t0
        else aux v  (succ k0) k  t0
  in
  match l with
  | [] -> assert false
  | v::t -> aux v 1 0 t

let list_max (l:'a list) : 'a * int =
  let rec aux v k0 k = function
    | [] -> (v, k)
    | v0::t0 ->
      if v0 > v
        then aux v0 (succ k0) k0 t0
        else aux v  (succ k0) k  t0
  in
  match l with
  | [] -> assert false
  | v::t -> aux v 1 0 t

let merge_uniq = SetList.union

let count_head x l =
  let rec aux x n = function
    | [] -> (n, [])
    | (h::t) as l -> if h = x
      then aux x (succ n) t
      else (n, l)
  in aux x 0 l

(* run length encoding
   [rle l = (pn, x) list]
   where [pn] is the length of the [x]-run minus 1
 *)
let rle
    ?(delta = (fun _ x -> x))
     (l:'a list) : (int * 'a) list =
  let rec aux carry c0 d0 n0 = function
    | [] -> List.rev ((n0, d0)::carry)
    | c1::t1 -> (
      if c1 = c0 then (
        aux carry  c0 d0 (succ n0) t1
      ) else (
        let d1 = delta (Some c0) c1 in
        let carry' = (n0, d0)::carry in
        aux carry' c1 d1     0     t1
      )
    )
  in
  match l with
  | [] -> []
  | c0::t0 -> aux [] c0 (delta None c0) 0 t0

(* run length decoding *)
let rld ?(delta = (fun _ x -> x)) (l:(int*'a) list) : 'a list =
  let rec aux carry op0 = function
    | [] -> List.rev carry
    | (n1, d1)::tail -> (
      let c1 = delta op0 d1 in
      aux (ntimes ~carry c1 (succ n1)) (Some c1) tail
    )
  in
  aux [] None l

let delta (diff:'a option -> 'a -> 'b) (l:'a list) : 'b list =
  let rec aux op carry = function
    | []   -> List.rev carry
    | a::t -> (
      aux (Some a) ((diff op a)::carry) t
    )
  in aux None [] l

let undelta (plus : 'a option -> 'b -> 'a) (l:'b list) : 'a list =
  let rec aux op carry = function
    | []   -> List.rev carry
    | b::t -> (
      let a = plus op b in
      aux (Some a) (a::carry) t
    )
  in aux None [] l

let delta_int (l:int list) : int list =
  let diff opx y = match opx with
    | None   -> y
    | Some x -> y-x
  in
  delta diff l

let undelta_int (l:int list) : int list =
  let plus opx y = match opx with
    | None   -> y
    | Some x -> y+x
  in
  undelta plus l

let delta'_int (l:int list) : int list =
  let diff opx y = match opx with
    | None   -> y
    | Some x -> y-(succ x)
  in
  delta diff l

let undelta'_int (l:int list) : int list =
  let plus opx y = match opx with
    | None   -> y
    | Some x -> y+(succ x)
  in
  undelta plus l

(* Longest Increasing Sub-Sequence *)
let liss ?(cmp=Stdlib.compare) (l:'a list) : int * (bool list) =
  let rec top0 n0 l0 = function
    | [] -> (n0, List.rev l0)
    | (_, n1, l1)::tail -> if n1 > n0
      then top0 n1 l1 tail
      else top0 n0 l0 tail
  in
  let top = function
    | [] -> (0, [])
    | (_, n0, l0)::tail -> top0 n0 l0 tail
  in
  let rec topmax a1 n1 l1 carry = function
    | [] -> ((a1, succ n1, true::l1)::(List.rev carry))
    | (a0, n0, l0)::tail ->
      let carry = (a0, n0, false::l0)::carry in
      if cmp a0 a1 <= 0 && n0 > n1
      then topmax a1 n0 l0 carry tail
      else topmax a1 n1 l1 carry tail
    in
  let rec main carry i = function
    | [] -> (
      top carry
    )
    | a::tail ->
      main (topmax a 0 (ntimes false i) [] carry) (succ i) tail
  in main [] 0 l

(* Longest Decreasing Sub-Sequence *)
let ldss ?(cmp=Stdlib.compare) (l:'a list) : int * (bool list) =
  liss ~cmp:(fun x y -> -(cmp x y)) l

(* Monotone Decomposition
  returns (seq, [(b, [x])]) where if b = false then [x] is increasing else (b = true) and [x] is decreasing
 *)
let monodecomp ?(cmp=Stdlib.compare) (l:int list)
    : int array * ((bool * (int list))list) =
  match l with
  | [] -> ([||], [])
  | liste -> (
    let status = Array.make (List.length liste) (-1) in
    let update_status x0 l =
      assert(x0>=0);
      let rec aux0 h i l =
        if status.(i) >= 0
        then aux0 h (succ i) l
        else (
          if h then (status.(i) <- x0);
          aux1 (succ i) l
        )
      and     aux1 i = function
        | [] -> ()
        | h::t -> aux0 h i t
      in
      aux1 0 l
    in
    let rec aux0 carry depth = function
      | [] ->
        (status, List.rev carry)
      | [x] -> (
        update_status depth [true];
        (status, List.rev ((false, [x])::carry))
      )
      | liste -> (
        let ni, li = liss ~cmp liste in
        let nd, ld = ldss ~cmp liste in
        if ni >= nd
          then ( (* we chose the increasing sequence *)
          let sub0, sub1 = bool_partition li liste in
          update_status depth li;
          aux0 ((false, sub1)::carry) (succ depth) sub0
        ) else ( (* we chose the decreasing sequence *)
          let sub0, sub1 = bool_partition ld liste in
          update_status depth ld;
          aux0 ((true , sub1)::carry) (succ depth) sub0
        )
      )
    in aux0 [] 0 liste
  )

(* Move Forward Encoding *)
let mfe (l:int list) : int list =
  let liste = ref [] in
  let find x =
    let rec aux0 carry i =
      assert(i<=x);
      if i = x
      then (x, x::(List.rev carry))
      else (aux0 (i::carry) (succ i))
    in
    let rec aux1 carry i = function
      | [] -> aux0 carry i
      | head::tail ->
        if head = x
        then (i, x::(List.rev_append carry tail))
        else (aux1 (head::carry) (succ i) tail)
    in
    let x, liste' = aux1 [] 0 !liste in
    liste := liste';
    x
  in
  List.map find l

(* Move Forward Decoding *)
let mfd (l:int list) : int list =
  let liste = ref [] in
  let find x =
    let rec aux0 carry i =
      assert(i<=x);
      if i = x
      then (x, x::(List.rev carry))
      else (aux0 (i::carry) (succ i))
    in
    let rec aux1 carry i = function
      | [] -> aux0 carry i
      | head::tail ->
        if i = x
        then (head, head::(List.rev_append carry tail))
        else (aux1 (head::carry) (succ i) tail)
    in
    let x, liste' = aux1 [] 0 !liste in
    liste := liste';
    x
  in
  List.map find l

let argmax cost ?(cmp=Stdlib.compare) x0 (l: _ list) =
  let rec aux x cx ix i = function
    | [] -> (x, cx, ix)
    | y::t -> (
      let cy = cost y in
      if cmp cy cx > 0
      then aux y cy i  (succ i) t
      else aux x cx ix (succ i) t
    )
  in aux x0 (cost x0) 0 0 l

let argmin cost ?(cmp=Stdlib.compare) x0 (l: _ list) =
  let rec aux x cx ix i = function
    | [] -> (x, cx, ix)
    | y::t -> (
      let cy = cost y in
      if cmp cy cx < 0
      then aux y cy i  (succ i) t
      else aux x cx ix (succ i) t
    )
  in aux x0 (cost x0) 0 0 l

let mapreduce liste (init:'a) fmap (fred:'a->'a->'a) : 'a =
  List.fold_left (fun c x -> fred c (fmap x)) init liste

let merge_uniq = SetList.union

let rec foldi_left ?(index=0) (f:int -> 'a -> 'b -> 'a) (a0:'a) (l:'b list) : 'a =
  match l with
  | [] -> a0
  | h::t -> foldi_left ~index:(succ index) f (f index a0 h) t

let opmax ?(cmp:'a->'a->int=Stdlib.compare) (al:'a list) : (int * 'a) option =
  foldi_left (fun i opx y ->
    match opx with
    | Some (_, x) when cmp x y >= 0 -> opx
    | _ -> Some(i, y)) None al

let opmin ?(cmp:'a->'a->int=Stdlib.compare) (al:'a list) : (int * 'a) option =
  foldi_left (fun i opx y ->
    match opx with
    | Some (_, x) when cmp x y <= 0 -> opx
    | _ -> Some(i, y)) None al

