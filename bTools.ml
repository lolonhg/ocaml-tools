module BChar = Internal_BChar

module BArray =
struct
  include Internal_BArray
  module Nat =
  struct
    type nat = t
    include Internal_BArray_Nat
  end
end

module OfB =
struct
  type stream = BinUtils.stream
  type 'a t = 'a BinUtils.load
  include Internal_OfB
end

module ToB =
struct
  type stream = BinUtils.stream
  type 'a t = 'a BinUtils.dump
  include Internal_ToB
end

module IoB =
struct
  type stream = BinUtils.stream
  type 'a t = 'a BinUtils.o3s
  type 'a b = 'a BinUtils.o3b
  include Internal_IoB
end

module OfBStream =
struct
  include Internal_OfBStream
end

type 'a br = 'a OfBStream.t

module ToBStream =
struct
  include Internal_ToBStream
end

type 'a bw = 'a ToBStream.t

type 'a b3 = 'a bw * 'a br
