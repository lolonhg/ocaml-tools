val flatten : 'a list list -> 'a list
val of_htbl : ('a, 'b) Hashtbl.t -> ('a * 'b) list
val to_htbl : ?size:int -> ('a * 'b) list -> ('a, 'b) Hashtbl.t
val merge_uniq : 'a list -> 'a list -> 'a list
val partition : ('a -> ('b, 'c) result ) -> 'a list -> 'c list * 'b list
val bool_partition : bool list -> 'a list -> 'a list * 'a list
val list_init : int -> (int -> 'a) -> 'a list
val init : int -> (int -> 'a) -> 'a list
val catlist : 'a -> 'a list list -> 'a list
val option_cons : 'a option -> 'a list -> 'a list
val catmap : ('a -> 'b list) -> 'a list -> 'b list
val list_of_oplist : 'a option list -> 'a list
val unop : 'a option list -> 'a list
val opmap : ('a -> 'b option) -> 'a list -> 'b list
val opmap2 : ('a -> 'b -> 'c option) -> 'a list -> 'b list -> 'c list
val sum : int list -> int
val count : ('a -> bool) -> 'a list -> int
val counti : (int -> 'a -> bool) -> 'a list -> int
val count_true : bool list -> int
val indexes : ('a -> bool) -> 'a list -> int list
val indexes_true : bool list -> int list
val indexesi : (int -> 'a -> bool) -> 'a list -> int list
val index : ('a -> bool) -> 'a list -> int option
val list_index : 'a -> 'a list -> int option
val ifind : ('a -> 'b option) -> 'a list -> (int * 'b) option

(* extract the longest prefix of [l] for which all elements satifiy predicate [p].
 *)
val find_prefix : ('a -> bool) -> 'a list ->'a list * 'a list

(* extract the longest prefix [pref] of [l] such that :
    - forall x\in l, f x = f(List.nth pref 0) (if pref <> [])
   The optional predicate [p] is only checked once on the first element, if its false returns [([], l)]
 *)
val find_onehash_prefix : ?p:('a -> bool) -> ('a -> 'b) -> 'a list -> 'a list * 'a list

val ntimes : ?carry:('a list) -> 'a -> int -> 'a list
val make : int -> 'a -> 'a list
val ncopy : 'a list -> int -> 'a list
val listfilter : bool list -> 'a list -> 'a list
val consensus0 : ('a0 -> 'a1 -> 'b * 'c0 option * 'c1 option) ->
   'a0 list -> 'a1 list -> 'b list * 'c0 list * 'c1 list
val consensusi0 : (int -> 'a0 -> 'a1 -> 'b * 'c0 option * 'c1 option) ->
   'a0 list -> 'a1 list -> 'b list * 'c0 list * 'c1 list
val consensus :
  ('a -> 'b * 'c -> 'd * 'e -> ('a * 'f) * ('b * 'g option) * ('d * 'h option)) ->
    'a -> 'b * 'c list -> 'd * 'e list -> ('a * 'f list) * ('b * 'g list) * ('d * 'h list)
val onehash_check : ('a -> 'b) -> 'b -> 'a list -> bool
val onehash : ('a -> 'b) -> 'a list -> bool
val get_onehash : ('a -> 'b) -> 'a list -> 'b option
val lists_length_check : int -> 'a list list -> bool
val lists_length : 'a list list -> bool
val lists_get_length : 'a list list -> int option
val nth_pop : 'a list -> int -> 'a option * ('a list)
val iremove : ('a -> 'b option) -> 'a list -> (int * 'b) option * ('a list)
val hdtl : 'a list -> 'a * 'a list
val map_hdtl : 'a list list -> 'a list * 'a list list
val hdtl_nth : int -> 'a list -> 'a list * 'a list
val list_add_partial_to_support : 'a -> 'a list list -> 'a list -> 'a list list
val list_transpose_partial_matrix : 'a -> int -> 'a list list -> 'a list list
val member : 'a -> 'a list -> bool
val int_of_boollist : bool list -> int
val list_uniq : 'a list -> 'a list
val string_of_list : ('a -> string) -> 'a list -> string
val print_list : ('a -> string) -> 'a list -> unit
val list_map_z1 : ('a -> 'a -> 'b) -> 'a list -> 'b list
val indexify : ('a -> bool) -> 'a list -> int option list
val indexify_true : bool list -> int option list
val foldmap : ('b -> 'a -> 'c * 'b) -> ('b -> bool) -> 'b -> 'a list -> 'c list * 'b
val opfoldmap : ('a -> 'b -> 'c option * 'b) -> ('b -> bool) -> 'b -> 'a list -> 'c list * 'b
val list_iterative_reduction : ('a -> 'b option * 'a option) -> 'a -> 'b list
val last : 'a list -> 'a list * 'a
val setnth : 'a list -> int -> 'a -> 'a list
val list_min : 'a list -> 'a * int
val list_max : 'a list -> 'a * int

val count_head : 'a -> 'a list -> int * 'a list
val rle : ?delta:('a option -> 'a -> 'a) -> 'a list -> (int * 'a) list
val rld : ?delta:('a option -> 'a -> 'a) -> (int * 'a) list -> 'a list
val delta : ('a option -> 'a -> 'b) -> 'a list -> 'b list
val undelta : ('a option -> 'b -> 'a) -> 'b list -> 'a list
val delta_int : int list -> int list
val undelta_int : int list -> int list
val delta'_int : int list -> int list
val undelta'_int : int list -> int list
val liss : ?cmp:('a -> 'a -> int) -> 'a list -> int * bool list
val ldss : ?cmp:('a -> 'a -> int) -> 'a list -> int * bool list
val monodecomp : ?cmp:(int -> int -> int) ->
  int list -> int array * (bool * int list) list
val mfe : int list -> int list
val mfd : int list -> int list
val argmax : ('a -> 'b) -> ?cmp:('b -> 'b -> int) ->
  'a -> 'a list -> 'a * 'b * int
val argmin : ('a -> 'b) -> ?cmp:('b -> 'b -> int) ->
  'a -> 'a list -> 'a * 'b * int
val mapreduce : 'a list -> 'b -> ('a -> 'b) -> ('b -> 'b -> 'b) -> 'b
val foldi_left : ?index:int -> (int -> 'a -> 'b -> 'a) -> 'a -> 'b list -> 'a
val opmax : ?cmp:('a -> 'a -> int) -> 'a list -> (int * 'a) option
val opmin : ?cmp:('a -> 'a -> int) -> 'a list -> (int * 'a) option
